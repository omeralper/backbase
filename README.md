# BackbaseProject

This assignment is submitted by Omer Alper Ozkan to "backbase" as a job interview task. 


As Zeynep Tunalioglu told me, I felt free to use Angular2. I hope using angular-cli wouldn't be problem for you. Angular-cli
is officially used by angular team for long time to create a project from scratch.  

To run the application:
 
npm i
npm start

the site will be published at localhost:4200

To test the application:

npm test



The application structured according to angular conventions. https://angular.io/guide/styleguide

I have only one module (app/weather), other than the main module. By default, the site redirects to /weather and shows
 5 cities' weather conditions.   

I have 3 components. WeatherListComponent is the start point, which router firstly redirects to it.
WeatherListComponent fetchs the data and WeatherListItemComponent's shows the data in the list.

WeatherDetailComponent is opened when user clicks on a city. It uses other API function to fetch detailed city weather info. In this
component I used a shim.d.ts to workaround a typescript global UMD problem. 

I have 1 API(app/APIs/WeatherAPI) with 2 functions. 
 
I tried to replicate http://openweathermap.org/ server's interfaces to make typescript more useful in WeatherAPI.

I tried to be simple at UX design. 

Bundling, simplifying, uglifying are done by angular-cli. I could have done these things manually to show my skills but automatization 
is nice thing :) I'd prefer to focus on code rather than devOps things. And as you know, npm start runs ng serve, which is development 
server. It could have done like: ng build --aot --prod. I am leaving it as is, for simplicity. 

Test codes might be extended more but It tests the main point and I tried to be simple.


Thank you for your time. 




