import {Component, Input} from '@angular/core';
import {CityWeather} from '../APIs/WeatherAPI';

@Component({
  selector: 'weather-list-item',
  templateUrl: './weather-list-item.component.html',
})
export class WeatherListItemComponent {
  @Input() cityWeather: CityWeather;
  Math: any;
  constructor() {
    this.Math = Math;
  }
}
