import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import {WeatherListItemComponent} from './weather-list-item.component';
import {RouterTestingModule} from '@angular/router/testing';
import 'rxjs/add/observable/of';
import {CityWeather} from '../APIs/WeatherAPI';

describe('Weather List Item Component', () => {

  let comp: WeatherListItemComponent;
  let fixture: ComponentFixture<WeatherListItemComponent>;
  let de: DebugElement;
  let el: HTMLElement;
  // emulates the API with some valid data.
  const cityWeatherStub: CityWeather = {
    'coord': {'lon': 37.62, 'lat': 55.75},
    'sys': {'type': 1, 'id': 7323, 'message': 0.0055, 'country': 'RU', 'sunrise': 1510289791, 'sunset': 1510320588},
    'weather': [{'id': 300, 'main': 'Drizzle', 'description': 'light intensity drizzle', 'icon': '09n'}, {
      'id': 701,
      'main': 'Mist',
      'description': 'mist',
      'icon': '50n'
    }],
    'main': {'temp': 2, 'pressure': 1013, 'humidity': 100, 'temp_min': 2, 'temp_max': 2},
    'visibility': 7000,
    'wind': {'speed': 4, 'deg': 160},
    'clouds': {'all': 90},
    'dt': 1510353582,
    'id': 524901,
    'name': 'Moscow'
  };
  // async beforeEach
  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: 'weather/:cityCode', component: WeatherListItemComponent}
        ])],
      declarations: [WeatherListItemComponent]
    })
      .compileComponents();
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherListItemComponent);

    comp = fixture.componentInstance;
    comp.cityWeather = cityWeatherStub;
    de = fixture.debugElement.query(By.css('h1'));
    el = de.nativeElement;
  });

  it('should show city info', () => {
    fixture.detectChanges();
    expect(el.textContent).toEqual('Moscow');
  });
});

