import {Component, OnInit} from '@angular/core';
import {MultipleCityWeather, WeatherAPI} from '../APIs/WeatherAPI';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'weather-list',
  templateUrl: './weather-list.component.html',
})
export class WeatherListComponent implements OnInit {
  // just some cities at Europe, is Moscow in Europe btw?
  cityCodes: Array<number> = [524901, 2934246, 2643743, 5107152, 4219762];
  cityWeathers: Observable<MultipleCityWeather>;

  constructor(public weatherAPI: WeatherAPI) {

  }

  ngOnInit() {
    this.cityWeathers = this.weatherAPI.getCityWeather(this.cityCodes);
  }
}
