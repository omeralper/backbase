import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import 'rxjs/add/observable/of';
import {WeatherDetailComponent} from './weather-detail.component';
import {CityWeatherDetailed, WeatherAPI} from '../APIs/WeatherAPI';
import {Observable} from 'rxjs/Observable';
import * as $ from 'jquery';

describe('Weather Detail Component', () => {

  let comp: WeatherDetailComponent;
  let fixture: ComponentFixture<WeatherDetailComponent>;
  let de: DebugElement;
  let el: HTMLElement;
  // async beforeEach
  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: 'weather/:cityCode', component: WeatherDetailComponent}
        ])],
      declarations: [WeatherDetailComponent],
      providers: [{provide: WeatherAPI, useValue: weatherAPIStub}]
    })
      .compileComponents();
  }));

  // synchronous beforeEach
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
    fixture = TestBed.createComponent(WeatherDetailComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('h1'));
    el = de.nativeElement;
  });

  it('should show city name correctly', () => {
    fixture.detectChanges();
    expect(el.textContent).toEqual('Kiev');
  });

  it('should get the city name correctly', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => { // wait for async getQuote
      fixture.detectChanges();        // update view with quote
      expect(el.textContent).toEqual('Kiev');
    });
  }));
});


const weatherAPIStub = {
  getCityWeatherForFiveDays: function (cityCode: number): Observable<CityWeatherDetailed> {
    return Observable.of({
      'message': 0.0036,
      'cnt': 40,
      'list': [{
        'dt': 1510358400,
        'main': {
          'temp': 6.53,
          'temp_min': 6.53,
          'temp_max': 6.82,
          'pressure': 1012.62,
          'sea_level': 1027.49,
          'grnd_level': 1012.62,
          'humidity': 99,
          'temp_kf': -0.29
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 92},
        'wind': {'speed': 4.66, 'deg': 205.502},
        'rain': {'3h': 0.19},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-11 00:00:00'
      }, {
        'dt': 1510369200,
        'main': {
          'temp': 6.53,
          'temp_min': 6.53,
          'temp_max': 6.75,
          'pressure': 1011.15,
          'sea_level': 1025.95,
          'grnd_level': 1011.15,
          'humidity': 100,
          'temp_kf': -0.21
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 80},
        'wind': {'speed': 5.02, 'deg': 199},
        'rain': {'3h': 0.2325},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-11 03:00:00'
      }, {
        'dt': 1510380000,
        'main': {
          'temp': 6.54,
          'temp_min': 6.54,
          'temp_max': 6.69,
          'pressure': 1010.3,
          'sea_level': 1025.06,
          'grnd_level': 1010.3,
          'humidity': 98,
          'temp_kf': -0.14
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10d'}],
        'clouds': {'all': 88},
        'wind': {'speed': 4.62, 'deg': 210.5},
        'rain': {'3h': 0.1225},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-11 06:00:00'
      }, {
        'dt': 1510390800,
        'main': {
          'temp': 8.52,
          'temp_min': 8.52,
          'temp_max': 8.6,
          'pressure': 1009,
          'sea_level': 1023.68,
          'grnd_level': 1009,
          'humidity': 100,
          'temp_kf': -0.07
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10d'}],
        'clouds': {'all': 0},
        'wind': {'speed': 4.86, 'deg': 215.504},
        'rain': {'3h': 0.065},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-11 09:00:00'
      }, {
        'dt': 1510401600,
        'main': {
          'temp': 9.36,
          'temp_min': 9.36,
          'temp_max': 9.36,
          'pressure': 1007.74,
          'sea_level': 1022.35,
          'grnd_level': 1007.74,
          'humidity': 90,
          'temp_kf': 0
        },
        'weather': [{'id': 802, 'main': 'Clouds', 'description': 'scattered clouds', 'icon': '03d'}],
        'clouds': {'all': 32},
        'wind': {'speed': 6.17, 'deg': 217.004},
        'rain': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-11 12:00:00'
      }, {
        'dt': 1510412400,
        'main': {
          'temp': 7.38,
          'temp_min': 7.38,
          'temp_max': 7.38,
          'pressure': 1007.49,
          'sea_level': 1022.13,
          'grnd_level': 1007.49,
          'humidity': 91,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 76},
        'wind': {'speed': 5.33, 'deg': 229.507},
        'rain': {'3h': 0.16},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-11 15:00:00'
      }, {
        'dt': 1510423200,
        'main': {
          'temp': 6.34,
          'temp_min': 6.34,
          'temp_max': 6.34,
          'pressure': 1007.33,
          'sea_level': 1022.14,
          'grnd_level': 1007.33,
          'humidity': 99,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 92},
        'wind': {'speed': 4.42, 'deg': 250.501},
        'rain': {'3h': 0.9775},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-11 18:00:00'
      }, {
        'dt': 1510434000,
        'main': {
          'temp': 5.21,
          'temp_min': 5.21,
          'temp_max': 5.21,
          'pressure': 1007.73,
          'sea_level': 1022.45,
          'grnd_level': 1007.73,
          'humidity': 98,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 44},
        'wind': {'speed': 5.01, 'deg': 268.501},
        'rain': {'3h': 0.595},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-11 21:00:00'
      }, {
        'dt': 1510444800,
        'main': {
          'temp': 2.29,
          'temp_min': 2.29,
          'temp_max': 2.29,
          'pressure': 1007.91,
          'sea_level': 1022.76,
          'grnd_level': 1007.91,
          'humidity': 100,
          'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01n'}],
        'clouds': {'all': 0},
        'wind': {'speed': 4.11, 'deg': 257.501},
        'rain': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-12 00:00:00'
      }, {
        'dt': 1510455600,
        'main': {
          'temp': 0.52,
          'temp_min': 0.52,
          'temp_max': 0.52,
          'pressure': 1007.92,
          'sea_level': 1022.77,
          'grnd_level': 1007.92,
          'humidity': 99,
          'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01n'}],
        'clouds': {'all': 0},
        'wind': {'speed': 3.97, 'deg': 217.5},
        'rain': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-12 03:00:00'
      }, {
        'dt': 1510466400,
        'main': {
          'temp': 1.1,
          'temp_min': 1.1,
          'temp_max': 1.1,
          'pressure': 1007.39,
          'sea_level': 1022.28,
          'grnd_level': 1007.39,
          'humidity': 97,
          'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01d'}],
        'clouds': {'all': 0},
        'wind': {'speed': 5.27, 'deg': 206.001},
        'rain': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-12 06:00:00'
      }, {
        'dt': 1510477200,
        'main': {
          'temp': 5.44,
          'temp_min': 5.44,
          'temp_max': 5.44,
          'pressure': 1006.73,
          'sea_level': 1021.44,
          'grnd_level': 1006.73,
          'humidity': 98,
          'temp_kf': 0
        },
        'weather': [{'id': 801, 'main': 'Clouds', 'description': 'few clouds', 'icon': '02d'}],
        'clouds': {'all': 20},
        'wind': {'speed': 5.46, 'deg': 209.006},
        'rain': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-12 09:00:00'
      }, {
        'dt': 1510488000,
        'main': {
          'temp': 5.81,
          'temp_min': 5.81,
          'temp_max': 5.81,
          'pressure': 1005.71,
          'sea_level': 1020.33,
          'grnd_level': 1005.71,
          'humidity': 94,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10d'}],
        'clouds': {'all': 56},
        'wind': {'speed': 6.06, 'deg': 219.002},
        'rain': {'3h': 0.07},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-12 12:00:00'
      }, {
        'dt': 1510498800,
        'main': {
          'temp': 4.47,
          'temp_min': 4.47,
          'temp_max': 4.47,
          'pressure': 1006.21,
          'sea_level': 1020.9,
          'grnd_level': 1006.21,
          'humidity': 97,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 88},
        'wind': {'speed': 5.96, 'deg': 225.003},
        'rain': {'3h': 0.995},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-12 15:00:00'
      }, {
        'dt': 1510509600,
        'main': {
          'temp': 3.73,
          'temp_min': 3.73,
          'temp_max': 3.73,
          'pressure': 1006.43,
          'sea_level': 1021.34,
          'grnd_level': 1006.43,
          'humidity': 97,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 24},
        'wind': {'speed': 3.46, 'deg': 228.501},
        'rain': {'3h': 0.02},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-12 18:00:00'
      }, {
        'dt': 1510520400,
        'main': {
          'temp': 0.29,
          'temp_min': 0.29,
          'temp_max': 0.29,
          'pressure': 1006.95,
          'sea_level': 1021.85,
          'grnd_level': 1006.95,
          'humidity': 98,
          'temp_kf': 0
        },
        'weather': [{'id': 801, 'main': 'Clouds', 'description': 'few clouds', 'icon': '02n'}],
        'clouds': {'all': 24},
        'wind': {'speed': 1.42, 'deg': 190.5},
        'rain': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-12 21:00:00'
      }, {
        'dt': 1510531200,
        'main': {
          'temp': -0.56,
          'temp_min': -0.56,
          'temp_max': -0.56,
          'pressure': 1006.7,
          'sea_level': 1021.6,
          'grnd_level': 1006.7,
          'humidity': 88,
          'temp_kf': 0
        },
        'weather': [{'id': 801, 'main': 'Clouds', 'description': 'few clouds', 'icon': '02n'}],
        'clouds': {'all': 24},
        'wind': {'speed': 2.42, 'deg': 130.001},
        'rain': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-13 00:00:00'
      }, {
        'dt': 1510542000,
        'main': {
          'temp': 1.97,
          'temp_min': 1.97,
          'temp_max': 1.97,
          'pressure': 1006.09,
          'sea_level': 1020.88,
          'grnd_level': 1006.09,
          'humidity': 96,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 92},
        'wind': {'speed': 3.4, 'deg': 137.5},
        'rain': {'3h': 0.19},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-13 03:00:00'
      }, {
        'dt': 1510552800,
        'main': {
          'temp': 3.79,
          'temp_min': 3.79,
          'temp_max': 3.79,
          'pressure': 1005.14,
          'sea_level': 1019.97,
          'grnd_level': 1005.14,
          'humidity': 100,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10d'}],
        'clouds': {'all': 92},
        'wind': {'speed': 5.16, 'deg': 144.51},
        'rain': {'3h': 0.24},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-13 06:00:00'
      }, {
        'dt': 1510563600,
        'main': {
          'temp': 6.53,
          'temp_min': 6.53,
          'temp_max': 6.53,
          'pressure': 1004.22,
          'sea_level': 1018.87,
          'grnd_level': 1004.22,
          'humidity': 100,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10d'}],
        'clouds': {'all': 64},
        'wind': {'speed': 5.16, 'deg': 173.501},
        'rain': {'3h': 0.0099999999999998},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-13 09:00:00'
      }, {
        'dt': 1510574400,
        'main': {
          'temp': 8.05,
          'temp_min': 8.05,
          'temp_max': 8.05,
          'pressure': 1003.79,
          'sea_level': 1018.39,
          'grnd_level': 1003.79,
          'humidity': 98,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10d'}],
        'clouds': {'all': 80},
        'wind': {'speed': 4.4, 'deg': 205.001},
        'rain': {'3h': 0.16},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-13 12:00:00'
      }, {
        'dt': 1510585200,
        'main': {
          'temp': 6.12,
          'temp_min': 6.12,
          'temp_max': 6.12,
          'pressure': 1004.39,
          'sea_level': 1018.88,
          'grnd_level': 1004.39,
          'humidity': 95,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 64},
        'wind': {'speed': 1.91, 'deg': 248.001},
        'rain': {'3h': 0.0099999999999998},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-13 15:00:00'
      }, {
        'dt': 1510596000,
        'main': {
          'temp': 4.52,
          'temp_min': 4.52,
          'temp_max': 4.52,
          'pressure': 1005.47,
          'sea_level': 1020.03,
          'grnd_level': 1005.47,
          'humidity': 95,
          'temp_kf': 0
        },
        'weather': [{'id': 803, 'main': 'Clouds', 'description': 'broken clouds', 'icon': '04n'}],
        'clouds': {'all': 80},
        'wind': {'speed': 3.24, 'deg': 311.503},
        'rain': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-13 18:00:00'
      }, {
        'dt': 1510606800,
        'main': {
          'temp': 4.37,
          'temp_min': 4.37,
          'temp_max': 4.37,
          'pressure': 1006.84,
          'sea_level': 1021.52,
          'grnd_level': 1006.84,
          'humidity': 100,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 92},
        'wind': {'speed': 4.31, 'deg': 340},
        'rain': {'3h': 0.09},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-13 21:00:00'
      }, {
        'dt': 1510617600,
        'main': {
          'temp': 3.42,
          'temp_min': 3.42,
          'temp_max': 3.42,
          'pressure': 1009.01,
          'sea_level': 1023.67,
          'grnd_level': 1009.01,
          'humidity': 100,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 92},
        'wind': {'speed': 4.28, 'deg': 353.001},
        'rain': {'3h': 0.05},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-14 00:00:00'
      }, {
        'dt': 1510628400,
        'main': {
          'temp': 2.68,
          'temp_min': 2.68,
          'temp_max': 2.68,
          'pressure': 1010.51,
          'sea_level': 1025.36,
          'grnd_level': 1010.51,
          'humidity': 100,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 88},
        'wind': {'speed': 4.74, 'deg': 354.003},
        'rain': {'3h': 0.010000000000001},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-14 03:00:00'
      }, {
        'dt': 1510639200,
        'main': {
          'temp': 2.31,
          'temp_min': 2.31,
          'temp_max': 2.31,
          'pressure': 1012.53,
          'sea_level': 1027.37,
          'grnd_level': 1012.53,
          'humidity': 100,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10d'}],
        'clouds': {'all': 92},
        'wind': {'speed': 5.3, 'deg': 355.005},
        'rain': {'3h': 0.02},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-14 06:00:00'
      }, {
        'dt': 1510650000,
        'main': {
          'temp': 3.41,
          'temp_min': 3.41,
          'temp_max': 3.41,
          'pressure': 1014.44,
          'sea_level': 1029.31,
          'grnd_level': 1014.44,
          'humidity': 100,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10d'}],
        'clouds': {'all': 92},
        'wind': {'speed': 5.52, 'deg': 357.5},
        'rain': {'3h': 0.03},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-14 09:00:00'
      }, {
        'dt': 1510660800,
        'main': {
          'temp': 4.3,
          'temp_min': 4.3,
          'temp_max': 4.3,
          'pressure': 1016.03,
          'sea_level': 1030.86,
          'grnd_level': 1016.03,
          'humidity': 98,
          'temp_kf': 0
        },
        'weather': [{'id': 803, 'main': 'Clouds', 'description': 'broken clouds', 'icon': '04d'}],
        'clouds': {'all': 80},
        'wind': {'speed': 5.74, 'deg': 350.504},
        'rain': {},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-14 12:00:00'
      }, {
        'dt': 1510671600,
        'main': {
          'temp': 2.88,
          'temp_min': 2.88,
          'temp_max': 2.88,
          'pressure': 1018.27,
          'sea_level': 1033.26,
          'grnd_level': 1018.27,
          'humidity': 95,
          'temp_kf': 0
        },
        'weather': [{'id': 803, 'main': 'Clouds', 'description': 'broken clouds', 'icon': '04n'}],
        'clouds': {'all': 80},
        'wind': {'speed': 4.26, 'deg': 348.5},
        'rain': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-14 15:00:00'
      }, {
        'dt': 1510682400,
        'main': {
          'temp': 2.62,
          'temp_min': 2.62,
          'temp_max': 2.62,
          'pressure': 1020.3,
          'sea_level': 1035.35,
          'grnd_level': 1020.3,
          'humidity': 97,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 76},
        'wind': {'speed': 4.27, 'deg': 350.001},
        'rain': {'3h': 0.029999999999999},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-14 18:00:00'
      }, {
        'dt': 1510693200,
        'main': {
          'temp': 2,
          'temp_min': 2,
          'temp_max': 2,
          'pressure': 1021.82,
          'sea_level': 1036.99,
          'grnd_level': 1021.82,
          'humidity': 97,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 88},
        'wind': {'speed': 3.48, 'deg': 357.503},
        'rain': {'3h': 0.065},
        'snow': {'3h': 0.025},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-14 21:00:00'
      }, {
        'dt': 1510704000,
        'main': {
          'temp': 1.61,
          'temp_min': 1.61,
          'temp_max': 1.61,
          'pressure': 1022.78,
          'sea_level': 1037.97,
          'grnd_level': 1022.78,
          'humidity': 95,
          'temp_kf': 0
        },
        'weather': [{'id': 600, 'main': 'Snow', 'description': 'light snow', 'icon': '13n'}],
        'clouds': {'all': 80},
        'wind': {'speed': 2.14, 'deg': 358.506},
        'rain': {},
        'snow': {'3h': 0.0325},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-15 00:00:00'
      }, {
        'dt': 1510714800,
        'main': {
          'temp': 1.08,
          'temp_min': 1.08,
          'temp_max': 1.08,
          'pressure': 1023.04,
          'sea_level': 1038.39,
          'grnd_level': 1023.04,
          'humidity': 96,
          'temp_kf': 0
        },
        'weather': [{'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n'}],
        'clouds': {'all': 80},
        'wind': {'speed': 1.45, 'deg': 351.501},
        'rain': {'3h': 0.0125},
        'snow': {'3h': 0.0425},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-15 03:00:00'
      }, {
        'dt': 1510725600,
        'main': {
          'temp': 0.9,
          'temp_min': 0.9,
          'temp_max': 0.9,
          'pressure': 1023.73,
          'sea_level': 1039.05,
          'grnd_level': 1023.73,
          'humidity': 98,
          'temp_kf': 0
        },
        'weather': [{'id': 600, 'main': 'Snow', 'description': 'light snow', 'icon': '13d'}],
        'clouds': {'all': 80},
        'wind': {'speed': 1.06, 'deg': 141.504},
        'rain': {},
        'snow': {'3h': 0.06},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-15 06:00:00'
      }, {
        'dt': 1510736400,
        'main': {
          'temp': 1.47,
          'temp_min': 1.47,
          'temp_max': 1.47,
          'pressure': 1023.99,
          'sea_level': 1039.22,
          'grnd_level': 1023.99,
          'humidity': 96,
          'temp_kf': 0
        },
        'weather': [{'id': 600, 'main': 'Snow', 'description': 'light snow', 'icon': '13d'}],
        'clouds': {'all': 44},
        'wind': {'speed': 2.27, 'deg': 188.502},
        'rain': {},
        'snow': {'3h': 0.0425},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-15 09:00:00'
      }, {
        'dt': 1510747200,
        'main': {
          'temp': 2.78,
          'temp_min': 2.78,
          'temp_max': 2.78,
          'pressure': 1022.78,
          'sea_level': 1037.95,
          'grnd_level': 1022.78,
          'humidity': 94,
          'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01d'}],
        'clouds': {'all': 0},
        'wind': {'speed': 2.62, 'deg': 207},
        'rain': {},
        'snow': {'3h': 0.01},
        'sys': {'pod': 'd'},
        'dt_txt': '2017-11-15 12:00:00'
      }, {
        'dt': 1510758000,
        'main': {
          'temp': -0.14,
          'temp_min': -0.14,
          'temp_max': -0.14,
          'pressure': 1021.96,
          'sea_level': 1037.14,
          'grnd_level': 1021.96,
          'humidity': 100,
          'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01n'}],
        'clouds': {'all': 0},
        'wind': {'speed': 2.31, 'deg': 190.504},
        'rain': {},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-15 15:00:00'
      }, {
        'dt': 1510768800,
        'main': {
          'temp': -2.34,
          'temp_min': -2.34,
          'temp_max': -2.34,
          'pressure': 1021.46,
          'sea_level': 1036.73,
          'grnd_level': 1021.46,
          'humidity': 92,
          'temp_kf': 0
        },
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '02n'}],
        'clouds': {'all': 8},
        'wind': {'speed': 3.06, 'deg': 195.505},
        'rain': {},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-15 18:00:00'
      }, {
        'dt': 1510779600,
        'main': {
          'temp': -2.67,
          'temp_min': -2.67,
          'temp_max': -2.67,
          'pressure': 1020.77,
          'sea_level': 1036.05,
          'grnd_level': 1020.77,
          'humidity': 97,
          'temp_kf': 0
        },
        'weather': [{'id': 802, 'main': 'Clouds', 'description': 'scattered clouds', 'icon': '03n'}],
        'clouds': {'all': 36},
        'wind': {'speed': 3.34, 'deg': 200.504},
        'rain': {},
        'snow': {},
        'sys': {'pod': 'n'},
        'dt_txt': '2017-11-15 21:00:00'
      }],
      'city': {'id': 703448, 'name': 'Kiev', 'coord': {'lat': 50.4333, 'lon': 30.5167}, 'country': 'UA'}
    });
  }
};
