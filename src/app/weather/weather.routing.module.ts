import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {WeatherListComponent} from './weather-list.component';
import {WeatherDetailComponent} from './weather-detail.component';

@NgModule({
  imports: [
    RouterModule.forChild([
        {
          path: 'weather',
          children: [
            {path: '', component: WeatherListComponent},
            {path: ':cityCode', component: WeatherDetailComponent}
          ]
        },
      ]
    )
  ],
  exports: [
    RouterModule
  ]
})
export class WeatherRoutingModule {
}
