import {NgModule} from '@angular/core';
import {WeatherListItemComponent} from './weather-list-item.component';
import {WeatherListComponent} from './weather-list.component';
import {WeatherRoutingModule} from './weather.routing.module';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {WeatherAPI} from '../APIs/WeatherAPI';
import {WeatherDetailComponent} from './weather-detail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    WeatherRoutingModule
  ],
  declarations: [
    WeatherListItemComponent,
    WeatherListComponent,
    WeatherDetailComponent
  ],
  providers: [WeatherAPI],
})
export class WeatherModule {
}
