/// <reference path="../../shim.d.ts" />

import {Component, ElementRef, OnInit} from '@angular/core';
import {CityWeatherDetailed, WeatherAPI} from '../APIs/WeatherAPI';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'weather-detail',
  templateUrl: './weather-detail.component.html',
})
export class WeatherDetailComponent implements OnInit {
  cityCode: number;
  cityWeather: Observable<CityWeatherDetailed>;
  Math: any;

  constructor(private weatherAPI: WeatherAPI, public route: ActivatedRoute, private el: ElementRef) {
    this.cityCode = this.route.snapshot.params['cityCode'];
    this.Math = Math;
  }

  ngOnInit() {
    this.cityWeather = this.weatherAPI
      .getCityWeatherForFiveDays(this.cityCode)
      .map((response: CityWeatherDetailed) => {
        this.drawGraph(response);
        return response;
      });
  }

  drawGraph(cityWeather: CityWeatherDetailed) {
    const container = $('.graphContainer', this.el.nativeElement);
    const temps = cityWeather.list.map((detailedWeather) => {
      return {
        x: new Date(detailedWeather.dt_txt).getTime(),
        y: detailedWeather.main.temp,
        marker: {
          symbol: 'url(http://openweathermap.org/img/w/' + detailedWeather.weather[0].icon + '.png)',
          icon: detailedWeather.weather[0].icon
        },
        customData: detailedWeather.weather
      };
    });

    const winds = cityWeather.list.map((detailedWeather) => {
      return {
        x: new Date(detailedWeather.dt_txt).getTime(),
        y: detailedWeather.wind.speed,
        customData: detailedWeather.weather
      };
    });

    const rainfalls = cityWeather.list.map((detailedWeather) => {
      return {
        x: new Date(detailedWeather.dt_txt).getTime(),
        y: detailedWeather.rain && detailedWeather.rain['3h'],
        customData: detailedWeather.weather
      };
    });

    const options = {
      chart: {
        zoomType: 'x'
      },
      title: {
        text: ''
      },
      xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
          month: '%e. %b',
          year: '%b'
        },
        title: {
          text: 'Date'
        }
      },
      yAxis: [
        { // Primary yAxis
          labels: {
            format: '{value}°C',
          },
          title: {
            text: 'Temperature',
          },
        },
        { // Wind speed
          gridLineWidth: 0,
          title: {
            text: 'Wind speed',
          },
          labels: {
            format: '{value} meter/sec',
          },
          opposite: true
        },
        { // Rain Fall
          title: {
            text: 'Rain Fall',
          },
          labels: {
            format: '{value} mm',
          },
          opposite: true
        }
      ],
      tooltip: {
        shared: true,
        useHTML: true,
        formatter: function () {
          let html = '';
          html += '<b>' + new Date(this.points[0].point.x).toLocaleString() + '</b>';
          html += '</br>';
          html += '<b>' + this.points[0].point.customData[0].description + '</b>';
          html += '</br>';
          this.points.forEach((point) => {
            switch (point.series.name) {
              case 'Wind Speed':
                html += '<b>Wind Speed</b> ';
                html += point.y + 'meter/sec';
                break;
              case 'Rain Fall':
                html += '<b>Rain Fall</b>  ';
                html += point.y + 'mm';
                break;
              case 'Temperature':
                html += '<b>Temperature</b>  ';
                html += point.y + '°C';
                html += '<img src="http://openweathermap.org/img/w/' + point.point.marker.icon + '.png" />';
                break;
            }
            html += '</br>';
          });
          return html;
        }
      },
      plotOptions: {
        spline: {
          marker: {
            enabled: true
          }
        }
      },
      series: [
        {
          name: 'Rain Fall',
          type: 'column',
          yAxis: 2,
          data: rainfalls
        },
        {
          name: 'Wind Speed',
          type: 'spline',
          yAxis: 1,
          data: winds
        },
        {
          name: 'Temperature',
          data: temps,
          type: 'spline'
        }]
    };

    const c = new Highcharts.Chart(container[0], options);
  }

}
