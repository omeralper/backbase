import {InjectionToken} from '@angular/core';

export let APP_CONFIG = new InjectionToken('app.config');

export interface IAppConfig {
  openWeatherMapServerAppId: string;
}

export const AppConfig: IAppConfig = {
  openWeatherMapServerAppId: 'bcb80685cca0c1bfa1c627fb5874db6b'
};
