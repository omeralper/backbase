import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {APP_CONFIG, IAppConfig} from '../app.config';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class WeatherAPI {

  static openWeatherMapServer = '//api.openweathermap.org';

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: IAppConfig) {

  }

  /**
   *
   * @param {Array<number>} cityCodes are here: http://bulk.openweathermap.org/sample/
   *
   * @returns {Observable<MultipleCityWeather>}
   * Returns weather conditions of multiple cities. By default I set units to 'metric', which makes server return Europen units
   * such like Celcius, meter, etc.
   */
  public getCityWeather(cityCodes: Array<number>): Observable<MultipleCityWeather> {
    const path = [WeatherAPI.openWeatherMapServer, '/data/2.5/group'].join('');
    return <Observable<MultipleCityWeather>>this.http
      .get(path, {
        params: {
          'id': cityCodes.join(','),
          'units': 'metric',
          'appid': this.config.openWeatherMapServerAppId
        }
      });
  }

  /**
   *
   * @param {number} cityCode are here:  http://bulk.openweathermap.org/sample/
   * @returns {Observable<CityWeatherDetailed>}
   * return detailed weather condition of a city of 5 days with 3 hours period. Supposed to return 40 items in the array.
   * By default I set units to 'metric', which makes server return Europen units such like Celcius, meter, etc.
   */
  public getCityWeatherForFiveDays(cityCode: number): Observable<CityWeatherDetailed> {
    const path = [WeatherAPI.openWeatherMapServer, '/data/2.5/forecast'].join('');
    return <Observable<CityWeatherDetailed>>this.http
      .get(path, {
        params: {
          'id': cityCode + '',
          'units': 'metric',
          'appid': this.config.openWeatherMapServerAppId
        }
      });
  }
}

/**
 * These interfaces are replication of http://openweathermap.org/ server APIs. There is no guarantee to fulfill required fields
 * at server side. I just assumed some fields are not nullable.
 */


/**
 * Based on the API here: http://openweathermap.org/current#parameter
 */
export interface MultipleCityWeather {
  /**
   * The length of the list
   */
  cnt: number;
  /**
   * List of cities weather status
   */
  list: Array<CityWeather>;
}

export interface CityWeather {
  /**
   * Coordinates of city
   */
  coord: CityCoord;
  /**
   * Weather conditions of the city. It can be multiple for specified period of time.
   */
  weather: Array<WeatherStatus>;
  /**
   * Main things about the current weather condition.
   */
  main: MainWeatherCondition;
  /**
   * Visibility range
   */
  visibility: number;
  /**
   * Wind condition
   */
  wind: Wind;
  /**
   * Cloud coverage on the sky by percentage
   */
  clouds: any;
  /**
   * DateTime of condition
   */
  dt: number;
  sys: any;
  id: number;
  /**
   * Name of the city
   */
  name: string;
}


/**
 * Wind condition
 */
export interface Wind {
  speed: number;
  deg: number;
}

/**
 * http://openweathermap.org/forecast5#parameter
 * Detailed weather information of one city. Server always(?) returns 40 condition for 5 days with 3 hours periods.
 */
export interface CityWeatherDetailed {
  /**
   * Brief info about city
   */
  city: CityData;
  /**
   * Length of the list
   */
  cnt: number;
  /**
   * Detailed weather info with 3 hour periods
   */
  list: Array<CityWeatherDetailedData>;
  /**
   * internal message
   */
  message: number;
}

/**
 * Brief data about city
 */
export interface CityData {
  /**
   * The id numbers are get from here http://bulk.openweathermap.org/sample/
   * The id number of the city
   */
  id: number;
  /**
   * Name of city
   */
  name: string;
  /**
   * lat, long coords
   */
  coord: CityCoord;
  /**
   * country code of city
   */
  country: string;
}

/**
 * Detailed weather conditions
 */
export interface CityWeatherDetailedData {
  /**
   * cloud coverage percentage
   */
  clouds: { all: number };
  /**
   * date time of condition
   */
  dt: number;
  /**
   * date time by string
   */
  dt_txt: string;
  /**
   * Main weather conditions
   */
  main: DetailedMainWeatherCondition;
  /**
   * Rain condition
   */
  rain?: RainSnowFall;
  /**
   * Snow condition
   */
  snow?: RainSnowFall;
  sys: any;
  /**
   * Detailed weather conditions
   */
  weather: Array<WeatherStatus>;
  /**
   * wind condition
   */
  wind: { speed: number, deg?: number };
}

/**
 * Rain or snow fall
 */
export interface RainSnowFall {
  '3h'?: number;
}

/**
 * Detailed Weather condition has some extra fields.
 */
export interface DetailedMainWeatherCondition extends MainWeatherCondition {
  sea_level: number;
  grnd_level: number;
  temp_kf: number;
}

/**
 * City coords
 */
export interface CityCoord {
  lon: number;
  lat: number;
}

/**
 * Weather status of city
 */
export interface WeatherStatus {
  id: number;
  main: string;
  description: string;
  icon: string;
}

/**
 * Main weather conditions of city
 */
export interface MainWeatherCondition {
  temp: number;
  pressure: number;
  humidity: number;
  temp_min: number;
  temp_max: number;
}
